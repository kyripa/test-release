
## 7.0.0 (2022-08-11)

### changed (2 changes)

- [prod test real second change](kyripa/test-release@96709c96563bad2861c11626aa87c9a3e12585e9) ([merge request](kyripa/test-release!28))
- [prod test second change](kyripa/test-release@596aa42057bbee52b0a544eede8e83bb81364ac7) ([merge request](kyripa/test-release!28))

## 6.0.5 (2022-08-11)

### changed (6 changes)

- [test second change](kyripa/test-release@db091009646ff7f8a2b17b73c0d72fdc2bac1438) ([merge request](kyripa/test-release!27))
- [test firs change](kyripa/test-release@d43a93b25c2e3f905544a02dd48d4c7ba8688b54) ([merge request](kyripa/test-release!27))
- [test second change](kyripa/test-release@ded228ee523d9aeccd6dc5c042298211d3420474) ([merge request](kyripa/test-release!26))
- [test firs change](kyripa/test-release@e9f6f076dd2c953af26179cabaf0d11d9ccfd428) ([merge request](kyripa/test-release!26))
- [test second change](kyripa/test-release@11c61f0a255d4107a9bb61db90797377b2e5639b) ([merge request](kyripa/test-release!25))
- [test firs change](kyripa/test-release@59ab7c8b5aac1849ccfb16dd8d6e6aa8b436ed37) ([merge request](kyripa/test-release!25))

## 6.0.2 (2022-08-11)

### removed (1 change)

- [removed red file](kyripa/test-release@76900e1e5ce56efb95491a260ab9abbcc8b97551) ([merge request](kyripa/test-release!24))

## 6.0.1 (2022-08-11)

### added (1 change)

- [Only MRs test](kyripa/test-release@3d96eda0624682527f11f385ad269eeec906c52e) ([merge request](kyripa/test-release!22))

## 6.0.0 (2022-08-11)

### changed (1 change)

- [test redfile](kyripa/test-release@a6dec9eaa78ee82486c02cb80ae1d396fe56cb6c) ([merge request](kyripa/test-release!21))

## 5.0.4 (2022-08-11)

### changed (5 changes)

- [test change log](kyripa/test-release@11aeced56ecef10e8f3528d99caeba30f696fa40) ([merge request](kyripa/test-release!20))
- [Update .gitlab-ci.yml file](kyripa/test-release@8cfa641b9b240799e4b71f0058e21b196b699a04)
- [Check createtion tags](kyripa/test-release@673019132f919dd96aefcf7aa2e495e6ab343d27) ([merge request](kyripa/test-release!19))
- [Merge branch 'test' into 'main'](kyripa/test-release@ee666cfe0ef704d483051711a35f540cc4b395ac) ([merge request](kyripa/test-release!17))
- [Update git vendor to gitlab](kyripa/test-release@cc5b73208168c7cca09bf3528d0ea5bbf7e18615) ([merge request](kyripa/test-release!17))

### fixed (2 changes)

- [Merge branch 'test' into 'main'](kyripa/test-release@a50d577b8c2da80b58b284c32a69d8f94d1f7f18) ([merge request](kyripa/test-release!18))
- [Update git vendor to gitlab](kyripa/test-release@501b977b2ef844b04b4fc0f4f422bb6cc9fb40d2) ([merge request](kyripa/test-release!18))

## 5.0.3 (2022-08-11)

### changed (3 changes)

- [Check createtion tags](kyripa/test-release@673019132f919dd96aefcf7aa2e495e6ab343d27) ([merge request](kyripa/test-release!19))
- [Merge branch 'test' into 'main'](kyripa/test-release@ee666cfe0ef704d483051711a35f540cc4b395ac) ([merge request](kyripa/test-release!17))
- [Update git vendor to gitlab](kyripa/test-release@cc5b73208168c7cca09bf3528d0ea5bbf7e18615) ([merge request](kyripa/test-release!17))

### fixed (2 changes)

- [Merge branch 'test' into 'main'](kyripa/test-release@a50d577b8c2da80b58b284c32a69d8f94d1f7f18) ([merge request](kyripa/test-release!18))
- [Update git vendor to gitlab](kyripa/test-release@501b977b2ef844b04b4fc0f4f422bb6cc9fb40d2) ([merge request](kyripa/test-release!18))

## 5.0.2 (2022-08-11)

### fixed (2 changes)

- [Merge branch 'test' into 'main'](kyripa/test-release@a50d577b8c2da80b58b284c32a69d8f94d1f7f18) ([merge request](kyripa/test-release!18))
- [Update git vendor to gitlab](kyripa/test-release@501b977b2ef844b04b4fc0f4f422bb6cc9fb40d2) ([merge request](kyripa/test-release!18))

### changed (2 changes)

- [Merge branch 'test' into 'main'](kyripa/test-release@ee666cfe0ef704d483051711a35f540cc4b395ac) ([merge request](kyripa/test-release!17))
- [Update git vendor to gitlab](kyripa/test-release@cc5b73208168c7cca09bf3528d0ea5bbf7e18615) ([merge request](kyripa/test-release!17))

## 5.0.1 (2022-08-11)

### changed (2 changes)

- [Merge branch 'test' into 'main'](kyripa/test-release@ee666cfe0ef704d483051711a35f540cc4b395ac) ([merge request](kyripa/test-release!17))
- [Update git vendor to gitlab](kyripa/test-release@cc5b73208168c7cca09bf3528d0ea5bbf7e18615) ([merge request](kyripa/test-release!17))

## 5.0.0 (2022-08-11)

No changes.

## 3.0.4 (2022-08-11)

No changes.

## 1.0.4 (2022-08-11)

No changes.

## 0.0.7 (2022-08-11)

No changes.

## 0.0.4 (2022-08-11)

No changes.

## 0.0.3 (2022-08-11)

No changes.

## 0.0.2 (2022-08-11)

No changes.

## 0.0.1 (2022-08-11)

No changes.
